## Node PA11Y configuration generator

_**Developed for Gitlab Hackathon, not intended to be used in production applications**_

Generates a pa11y config file based on URL and device, or a js string file to highlight all accessibility elements based on json outputs of pa11y.

Install using 
`npm i -g pally-config`

Usage below:

```bash
pally-config --help
Generate config.json to pass to Pally or js for passing to a screenshot utility to capture A11y
issues

Commands:
  pally-config config     Generates a config.json file to pass to
                          PA11y as a configuration for pally reporting
  pally-config highlight  Generates a screenshotter js command from an A11y
                          output report to execute and highlight elements before
                          taking screens

Options:
  --help     Show help                                                 [boolean]
  --version  Show version number                                       [boolean]
```

