#!/usr/bin/env node
const yargs = require('yargs')
const jsonfile = require('jsonfile')
const processor = require("./index")
const fs = require("fs")

yargs
.usage("Generate config.json or js for passing to a screenshot utility to capture A11y issues")
.showHelpOnFail(true, 'Specify --help for available options')
.command({ 
    command: 'config', 
    describe: 'Generate config.json to pass to Pa11y based on device and URL', 
    builder: { 
        report: { 
            describe: 'A11y url and device used to generate config file (JSON)', 
            demandOption: true,  // Required 
            type: 'string',
            alias: "r"     
        }, 
        device: { 
            describe: 'A11y testing device', 
            demandOption: true,  // One of mobile, tablet, desktop
            choices: ["desktop","mobile","tablet"],
            type: 'string',
            alias: "d"     
        }, 
        output: {   
            describe: 'Output config.json filepath', 
            demandOption: false, 
            default: "./config.json",
            type: 'string',
            alias: "o"
        } 
    }, 
  
    // Function for your command 
    handler(argv) {
        let report = jsonfile.readFileSync(argv.report)
        let config = processor.generateConfig(report,argv.device)
        jsonfile.writeFileSync(argv.output,config)
    } 
}).command({ 
    command: 'highlight', 
    describe: 'Generates a screenshotter js command from an A11y output report to execute and highlight elements before taking screens', 
    builder: { 
        report: { 
            describe: 'A11y output report', 
            demandOption: true,  // Required 
            type: 'string',
            alias: "r"     
        }, 
        output: {   
            describe: 'Output config.json filepath', 
            demandOption: false, 
            default: "./highlight.js",
            type: 'string',
            alias: "o"
        }
    }, 
  
    // Function for your command 
    handler(argv) { 
        let report = jsonfile.readFileSync(argv.report)
        let js = processor.generateJs(report)
        fs.writeFileSync(argv.output,js,{encoding:"utf8"})
    } 
}).parse()