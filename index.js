function getDefaultObject(){
    let obj = {}
    obj.defaults = {}
    let chromeLaunchConfig = {}
    chromeLaunchConfig.args = ["--no-sandbox"]
    chromeLaunchConfig.ignoreHTTPSErrors = false
    obj.defaults.chromeLaunchConfig = chromeLaunchConfig

    obj.defaults.timeout = 60000
    obj.defaults.level = "none"
    obj.defaults.threshold = 1
    obj.defaults.reporters = "json"
    return obj
}

function getUserAgent(device){
    switch(device){
        case "mobile":
            return  "Mozilla/5.0 (iPhone; CPU iPhone OS 16_5 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/114.0.5735.99 Mobile/15E148 Safari/604.1"
        case "desktop":
            return "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36"
        case "tablet":
            return "Mozilla/5.0 (iPad; CPU OS 17_3_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.2 Mobile/15E148 Safari/604.1"
        default:
            return "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36"
    }
}

function getScreenSize(device){
    switch(device){
        case "mobile":
            return  {width: 320,height: 720}
        case "desktop":
            return  {width: 1920,height: 1080}
        case "tablet":
            return  {width: 768,height: 946}
        default:
            return  {width: 1920,height: 1080}
    }
}

module.exports.generateConfig = function(report,device){
    let obj = getDefaultObject()
    let urlObject = {}
    let urlFromReport = Object.keys(report["results"])[0]
    urlObject.url = urlFromReport
    urlObject.viewport = getScreenSize(device)
    urlObject.userAgent = getUserAgent(device)
    obj.urls = [urlObject]
    return obj
}

module.exports.generateJs = function(report){
    if (report["errors"] < 1){
        return `console.log("No elements to highlight")`
    }
    let issues = Object.values(report["results"])[0]
    let statements = issues.filter(e=>e.type === "error").map(e=>{
        let selector = e.selector
        let command = `document.querySelector("${selector}").style.border = "thick dashed #FF0000";`
        command = command + `document.querySelector("${selector}").style.boxShadow = "inset rgba(255, 0, 0, 0.5) 0px 0px 40px 0px, inset rgba(255, 70, 85, 0.5) 0px 0px 40px 0px, inset rgba(0, 0, 0, 1) 0em 0em 1em -1em";`
        return command
    })
    return statements.join("")
}
